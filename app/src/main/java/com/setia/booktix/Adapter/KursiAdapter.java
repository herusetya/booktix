package com.setia.booktix.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.setia.booktix.Activity.LatKursi;
import com.setia.booktix.Model.KursiModel;
import com.setia.booktix.R;

import java.util.ArrayList;

public class KursiAdapter extends RecyclerView.Adapter<KursiAdapter.ViewHolder> {
    private LatKursi mContext;
    private ArrayList<KursiModel> mList;
    private Boolean clicked = false;
    private int pos = 0;
    private CardView v;

    public KursiAdapter(LatKursi mContext, ArrayList<KursiModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    @NonNull
    @Override
    public KursiAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.lat_kursi_list, viewGroup, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull KursiAdapter.ViewHolder viewHolder, final int position) {
        final KursiModel kursiModel = mList.get(position);
        TextView nomorKursi;
        final CardView cardKursi;

        cardKursi = viewHolder.cardKursi;
        nomorKursi = viewHolder.nomorKursi;
        nomorKursi.setText(kursiModel.getNomorKursi());

        cardKursi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardKursi.setCardBackgroundColor(Color.parseColor("#32CD32"));
                mContext.NO_KRS = kursiModel.getNomorKursi();
                mContext.loadTambahnoKrs();
                mContext.tambahAg();
                /*
                if(clicked == false){
                    cardKursi.setCardBackgroundColor(Color.parseColor("#32CD32"));
                    clicked = false;
                    mContext.NO_KRS = kursiModel.getNomorKursi();
                    mContext.loadTambahnoKrs();
                    mContext.tambahAg();
                }
                //////////////////////////////////////
                pos = position;
                if(v != null){
                    if(v != view){
                        v.setCardBackgroundColor(Color.parseColor("#D3D3D3"));
                    }
                }
                if(position == pos){
                    cardKursi.setCardBackgroundColor(Color.parseColor("#32CD32"));
                    v = (CardView) view;
                }
                */
            }
        });

    }
    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nomorKursi;
        CardView cardKursi;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nomorKursi = itemView.findViewById(R.id.namaKursi);
            cardKursi = itemView.findViewById(R.id.cardKursi);
        }
    }

}
