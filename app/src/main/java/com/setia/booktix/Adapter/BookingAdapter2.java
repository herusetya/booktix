package com.setia.booktix.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.setia.booktix.Activity.LatDetailPemesananTiket;
import com.setia.booktix.Database.Database2;
import com.setia.booktix.Fragment.PesanFragment;
import com.setia.booktix.Model.DtailTiketModel;
import com.setia.booktix.R;

import java.util.List;

public class BookingAdapter2 extends RecyclerView.Adapter<BookingAdapter2.ViewHolder> {
    private PesanFragment pesanFragment;
    private List<DtailTiketModel> mList;
    private Database2 dbt;
    private Context mContext2;
    private Context tempContext;

    public BookingAdapter2(Context mContext, List<DtailTiketModel> mList) {
        this.mContext2 = mContext;
        this.mList = mList;
    }

    public BookingAdapter2(PesanFragment fPesan, List<DtailTiketModel> mList){
        this.pesanFragment = fPesan;
        this.mList = mList;
    }

    @NonNull
    @Override
    public BookingAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        tempContext = pesanFragment == null ? mContext2 : pesanFragment.getContext();
        View view = LayoutInflater.from(tempContext).inflate(R.layout.lat_menu_pemesanan_list,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookingAdapter2.ViewHolder viewHolder, final int i) {
        final DtailTiketModel model = mList.get(i);
        final RelativeLayout relativeLayout;
        final Button btnHapus;
        final TextView txtidbooking,txtjudulfilm,txttanggal,txtwaktu,txtstudio,txtkursi,txtjmlkursi;

        dbt = new Database2(tempContext);

        txtidbooking = viewHolder.idbooking1;
        txtjudulfilm = viewHolder.judulfilm1;
        txttanggal = viewHolder.tanggal1;
        txtwaktu = viewHolder.waktu1;
        txtstudio = viewHolder.studio1;
        txtkursi = viewHolder.kursi1;
        txtjmlkursi = viewHolder.jmlkursi1;
        btnHapus = viewHolder.btnHapus1;
        relativeLayout = viewHolder.rl1;

        txtidbooking.setText(model.getIdbooking());
        txtjudulfilm.setText(model.getJudulfilm());
        txttanggal.setText(model.getTanggal());
        txtwaktu.setText(model.getWaktu());
        txtstudio.setText(model.getStudio());
        txtkursi.setText(model.getKursi());
        txtjmlkursi.setText(model.getJmlkursi());

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(tempContext,LatDetailPemesananTiket.class);
                intent.putExtra("idbooking",model.getIdbooking());
                intent.putExtra("judulfilm",model.getJudulfilm());
                intent.putExtra("tanggal",model.getTanggal());
                intent.putExtra("waktu",model.getWaktu());
                intent.putExtra("studio",model.getStudio());
                intent.putExtra("kursi",model.getKursi());
                intent.putExtra("jmlkursi",model.getJmlkursi());
                tempContext.startActivity(intent);


            }
        });
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbt.deleteByid(model.getIdbooking());
                Toast.makeText(tempContext, "Dihapus", Toast.LENGTH_SHORT).show();
                pesanFragment.setPesan();
            }

        });


    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView idbooking1,judulfilm1,tanggal1,waktu1,studio1,kursi1,jmlkursi1;
        RelativeLayout rl1;
        Button btnHapus1;
        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            rl1 = itemView.findViewById(R.id.rel1);
            idbooking1 = itemView.findViewById(R.id.tamidbooking);
            judulfilm1 = itemView.findViewById(R.id.tamjudultampil);
            tanggal1 = itemView.findViewById(R.id.tamtanggal);
            waktu1 = itemView.findViewById(R.id.tamwaktu);
            studio1 = itemView.findViewById(R.id.tamstudio);
            kursi1 = itemView.findViewById(R.id.tamkursi);
            jmlkursi1 = itemView.findViewById(R.id.tamjmlkursi);
            btnHapus1 = itemView.findViewById(R.id.btnhapus);

        }

    }

}
