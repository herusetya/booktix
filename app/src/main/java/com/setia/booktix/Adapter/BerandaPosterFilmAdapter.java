package com.setia.booktix.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.setia.booktix.Activity.LatJadwal;
import com.setia.booktix.Fragment.BerandaFragment;
import com.setia.booktix.Model.PosterFilmModel;
import com.setia.booktix.R;

import java.util.ArrayList;

public class BerandaPosterFilmAdapter extends RecyclerView.Adapter<BerandaPosterFilmAdapter.ViewHolder> {
    private Context mContext;
    BerandaFragment berandaFragment;
    private ArrayList<PosterFilmModel> mList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void  setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public BerandaPosterFilmAdapter(Context mContext, ArrayList<PosterFilmModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }
    @NonNull
    @Override
    public BerandaPosterFilmAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.lat_menu_beranda, viewGroup, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull BerandaPosterFilmAdapter.ViewHolder viewHolder, int position) {
        final PosterFilmModel posterFilmModel = mList.get(position);

        LinearLayout linear,linearDesc;
        final TextView jfilm,textDet;
        ImageView imagePoster;
        final BerandaFragment brd = new BerandaFragment();

        imagePoster = viewHolder.image;
        linear = viewHolder.linearLayout;
        jfilm = viewHolder.judulFilm_2;
        jfilm.setText(posterFilmModel.getJudulFilm());
        textDet = viewHolder.textDetail;

        Glide.with(mContext)
                .load(posterFilmModel.getGambar())
                .into(imagePoster);

        linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,LatJadwal.class);
                intent.putExtra("gambar",posterFilmModel.getGambar());
                intent.putExtra("judul",posterFilmModel.getJudulFilm());
                intent.putExtra("kategori",posterFilmModel.getKategori());
                intent.putExtra("durasi",posterFilmModel.getDurasi());
                intent.putExtra("jadwal",posterFilmModel.getJadwal());
                intent.putExtra("studio",posterFilmModel.getStudio());
                intent.putExtra("iduser",posterFilmModel.getId_user());
                intent.putExtra("username",posterFilmModel.getUsername());
                mContext.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView judulFilm_2,textDetail;
        CardView cardposter;
        LinearLayout linearLayout,linearDeskripsi;
        ImageView image;
        Boolean clicked = false;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.cardposter2);
            linearDeskripsi = itemView.findViewById(R.id.linearDeskripsi);
            cardposter = itemView.findViewById(R.id.cardposter1);
            judulFilm_2 = itemView.findViewById(R.id.judulFilm_2);
            linearLayout = itemView.findViewById(R.id.linearPoster);
            textDetail = itemView.findViewById(R.id.textDetail);

        }
    }
}
