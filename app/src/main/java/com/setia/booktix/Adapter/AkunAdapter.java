package com.setia.booktix.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.setia.booktix.Activity.MenuAkunActivity;
import com.setia.booktix.Model.AkunModel;
import com.setia.booktix.R;

import java.util.ArrayList;

public class AkunAdapter extends RecyclerView.Adapter<AkunAdapter.ViewHolder>{
    private final Context mContext;
    ArrayList<AkunModel> mList;

    public AkunAdapter(Context mContext, ArrayList<AkunModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.lat_menu_akun, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AkunAdapter.ViewHolder viewHolder, int position) {
        final AkunModel akunModel = mList.get(position);
        final RelativeLayout relativeLayout;
        final TextView id_user,username,password,nama,nomorhp;
        Button EDIT;
        id_user = viewHolder.txtid_user;
        username = viewHolder.txtusername;
        password = viewHolder.txtpassword;
        nama = viewHolder.txtnama;
        nomorhp = viewHolder.txtnomorhp;
        relativeLayout = viewHolder.rvAkuns;
        EDIT = viewHolder.edt_edit;
        id_user.setText(akunModel.getId_user());
        username.setText(akunModel.getUsername());
        password.setText(akunModel.getPassword());
        nama.setText(akunModel.getNama());
        nomorhp.setText(akunModel.getNomorhp());
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        EDIT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext,MenuAkunActivity.class);
                intent.putExtra("id_user",akunModel.getId_user());
                intent.putExtra("username",akunModel.getUsername());
                intent.putExtra("password",akunModel.getPassword());
                intent.putExtra("nama",akunModel.getNama());
                intent.putExtra("nomorhp",akunModel.getNomorhp());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtid_user,txtusername,txtpassword,txtnama,txtnomorhp;
        RelativeLayout rvAkuns;
        Button edt_edit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            rvAkuns = itemView.findViewById(R.id.akuns);
            edt_edit = itemView.findViewById(R.id.btn_edt);
            txtid_user = itemView.findViewById(R.id.txt_id_user);
            txtusername = itemView.findViewById(R.id.txt_username);
            txtpassword = itemView.findViewById(R.id.txt_password);
            txtnama = itemView.findViewById(R.id.txt_nama);
            txtnomorhp = itemView.findViewById(R.id.txt_nomorhp);
        }
    }
}
