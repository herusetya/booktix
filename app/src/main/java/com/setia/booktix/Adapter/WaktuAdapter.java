package com.setia.booktix.Adapter;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.setia.booktix.Activity.LatJadwal;
import com.setia.booktix.Model.WaktuModel;
import com.setia.booktix.R;

import java.util.ArrayList;

public class WaktuAdapter extends RecyclerView.Adapter<WaktuAdapter.ViewHolder> {
    private LatJadwal mContext;
    private ArrayList<WaktuModel> mList;
    private Boolean diklik = false;
    private OnItemClickListener mListener;
    private int pos = 0;
    private CardView v;

    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void  setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public WaktuAdapter(LatJadwal mContext, ArrayList<WaktuModel>mlist){
        this.mContext = mContext;
        this.mList = mlist;
    }

    @NonNull
    @Override
    public WaktuAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.lat_jadwal_waktu, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final WaktuAdapter.ViewHolder viewHolder, final int position) {
        final WaktuModel waktuModel = mList.get(position);
        final TextView WaktuTayang;
        final CardView waktu;

        waktu = viewHolder.card_waktuFilm;
        WaktuTayang = viewHolder.WaktuFilm;
        WaktuTayang.setText(waktuModel.getWaktuTayang());
        waktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pos = position;

                if(v != null){
                    if(v != view){
                        v.setCardBackgroundColor(Color.parseColor("#D3D3D3"));
                    }
                }

                if(position == pos){
                    waktu.setCardBackgroundColor(Color.parseColor("#32CD32"));
                    mContext.time = waktuModel.getWaktuTayang();
                    v = (CardView) view;
                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder  {
        TextView WaktuFilm;
        CardView card_waktuFilm;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            WaktuFilm = itemView.findViewById(R.id.txtWaktuFilm);
            card_waktuFilm = itemView.findViewById(R.id.cardwaktuFilm);
        }
    }
}
