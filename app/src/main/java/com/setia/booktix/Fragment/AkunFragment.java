package com.setia.booktix.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.setia.booktix.Activity.LoginActivity;
import com.setia.booktix.Adapter.AkunAdapter;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.Model.AkunModel;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AkunFragment extends Fragment {

    ArrayList<AkunModel> modelArrayList;
    RecyclerView recyclerView;
    String url = Server.URL_AKUN;
    AkunAdapter akunAdapter;
    private String tag_json_obj = "json_obj_req";
    Button btn_logout;
    TextView txt_id, txt_username, txt_password;
    String iduser, username;
    SharedPreferences sharedpreferences;
    public static final String TAG_ID = "id_user";
    public static final String TAG_USERNAME = "username";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_akun, container, false);

        btn_logout = (Button) rootView.findViewById(R.id.btn_logout);
        sharedpreferences = getActivity().getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);
        iduser = getActivity().getIntent().getStringExtra(TAG_ID);
        username = getActivity().getIntent().getStringExtra(TAG_USERNAME);
        
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // update login session ke FALSE dan mengosongkan nilai id dan username
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(LoginActivity.session_status, false);
                editor.putString(TAG_ID, null);
                editor.putString(TAG_USERNAME, null);
                editor.commit();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                getActivity().finish();
                startActivity(intent);
            }
        });
        recyclerView = rootView.findViewById(R.id.dataAkun);
        modelArrayList = new ArrayList<>();
        loadAkun();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1,GridLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        akunAdapter = new AkunAdapter(getActivity(),modelArrayList);
        recyclerView.setAdapter(akunAdapter);
        return rootView;
    }
    private void loadAkun() {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            AkunModel model = new AkunModel();
                            model.setId_user(data.getString( "id_user" ));
                            model.setUsername( data.getString( "username" ) );
                            model.setPassword( data.getString( "password" ) );
                            model.setNama (data.getString("nama"));
                            model.setNomorhp(data.getString("nomorhp"));
                            model.setFoto(data.getString("foto"));
                            modelArrayList.add(model);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    akunAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_user",iduser);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);
    }
}
