package com.setia.booktix.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.setia.booktix.Activity.LatJadwal;
import com.setia.booktix.Activity.LatPesanManual;
import com.setia.booktix.Activity.LatKursi;
import com.setia.booktix.Activity.LatPembayaran;
import com.setia.booktix.R;

public class BantuanFragment extends Fragment {
    Button btncobakursi,btncobajadwal,btncobabooking,btncobabooking1,btncobauploadgambar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bantuan, container, false);
        btncobakursi = rootView.findViewById(R.id.btn_cobakursi);
        btncobajadwal = rootView.findViewById(R.id.btn_cobajadwal);
        btncobabooking1 = rootView.findViewById(R.id.btn_cobabooking1);
        btncobauploadgambar = rootView.findViewById(R.id.btn_cobauploadgambar);

        btncobakursi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LatKursi.class);
                startActivity(intent);
            }
        });
        btncobajadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LatJadwal.class);
                startActivity(intent);
            }
        });
        btncobabooking1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LatPesanManual.class);
                startActivity(intent);
            }
        });
        btncobauploadgambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),LatPembayaran.class);
                startActivity(intent);

            }
        });
        return rootView;
    }
}

