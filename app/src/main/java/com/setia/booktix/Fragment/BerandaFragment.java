package com.setia.booktix.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.setia.booktix.Activity.LoginActivity;
import com.setia.booktix.Adapter.BerandaPosterFilmAdapter;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.Model.PosterFilmModel;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class BerandaFragment extends Fragment{
    RecyclerView recyclerView;
    ArrayList<PosterFilmModel> modelArrayList;
    String url = Server.URL_FILM;
    BerandaPosterFilmAdapter posterFilmAdapter;
    private String tag_json_obj = "json_obj_req";

    public String iduser, username;
    SharedPreferences sharedpreferences;
    public static final String TAG_ID = "id_user";
    public static final String TAG_USERNAME = "username";
    String tanggal;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_beranda, container, false);
        sharedpreferences = getActivity().getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);
        iduser = getActivity().getIntent().getStringExtra(TAG_ID);
        username = getActivity().getIntent().getStringExtra(TAG_USERNAME);
        Calendar calendar = Calendar.getInstance();
        tanggal = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.getTime());
        /////////////////////////
        recyclerView = rootView.findViewById(R.id.posterfilm_1);
        modelArrayList = new ArrayList<>();
        loadFilm();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),1,GridLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        posterFilmAdapter = new BerandaPosterFilmAdapter(getActivity(),modelArrayList);
        recyclerView.setAdapter(posterFilmAdapter);
        /////////////////////////
        return rootView;
    }

    private void loadFilm() {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            PosterFilmModel model = new PosterFilmModel();
                            System.out.println("JUDUL : " + data.getString("judul"));
                            model.setJudulFilm( data.getString( "judul" ) );
                            model.setKategori( data.getString( "kategori" ) );
                            model.setDurasi( data.getString( "durasi" ) );
                            model.setGambar( data.getString( "gambar" ) );
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setJadwal( data.getString( "jadwal" ) );
                            model.setStudio( data.getString( "studio" ) );
                            model.setId_user(iduser);
                            model.setUsername(username);
                            modelArrayList.add(model);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    posterFilmAdapter.notifyDataSetChanged();

                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } );
        /*{
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("jadwal",tanggal);
                return params;
            }
        };*/
        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);
    }
}

