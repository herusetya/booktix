package com.setia.booktix.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.setia.booktix.Activity.LoginActivity;
import com.setia.booktix.Adapter.BookingAdapter2;
import com.setia.booktix.Database.Database2;
import com.setia.booktix.R;

public class PesanFragment extends Fragment{
    private RecyclerView rv1;
    private BookingAdapter2 bookingAdapter2;
    private String iduser, username;
    SharedPreferences sharedpreferences;
    public static final String TAG_ID = "id_user";
    public static final String TAG_USERNAME = "username";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pemesanan, container, false);
        rv1 = rootView.findViewById(R.id.rv_booking1);
        setPesan();
        sharedpreferences = getActivity().getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);
        iduser = getActivity().getIntent().getStringExtra(TAG_ID);
        username = getActivity().getIntent().getStringExtra(TAG_USERNAME);
        return rootView;
    }

    public void setPesan() {
        Database2 db = new Database2(getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager layoutManager1 = layoutManager;
        rv1.setNestedScrollingEnabled(false);
        rv1.setLayoutManager(layoutManager1);
        rv1.setHasFixedSize(true);
        bookingAdapter2 = new BookingAdapter2(PesanFragment.this, db.getBook());
        rv1.setAdapter(bookingAdapter2);
    }
}

