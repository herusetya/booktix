package com.setia.booktix.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.setia.booktix.Model.BookingModel;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {
    private static final String DB_NAME = "booktix.db";
    private static final int DB_VER = 1;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);

    }

    public List<BookingModel> getBook() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"iduser", "idkursi", "idfilm"};
        String table = "keranjang";

        qb.setTables(table);
        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);

        final List<BookingModel> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                result.add(new BookingModel(c.getString(c.getColumnIndex("iduser")),
                        c.getString(c.getColumnIndex("idkursi")),
                        c.getString(c.getColumnIndex("idfilm"))
                ));
            } while (c.moveToNext());
        }

        return result;
    };

    public void addBook(BookingModel bookingModel)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format("INSERT INTO keranjang(iduser,idkursi,idfilm) VALUES('%s','%s','%s');",
                bookingModel.getIduser(),bookingModel.getIdkursi(),bookingModel.getIdfilm());
        db.execSQL(query);
    }
    public void delete()
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM keranjang");
        db.execSQL(query);
    }

    public void deleteByKey(String key)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM keranjang WHERE idkursi='" + key + "'";
        db.execSQL(query);
    }
}


