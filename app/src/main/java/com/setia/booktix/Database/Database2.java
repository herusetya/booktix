package com.setia.booktix.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
import com.setia.booktix.Model.DtailTiketModel;

import java.util.ArrayList;
import java.util.List;

public class Database2 extends SQLiteAssetHelper {
    private static final String DB_NAME = "bookingDT.db";
    private static final int DB_VER = 1;

    public Database2(Context context) {
        super(context, DB_NAME, null, DB_VER);

    }

    public List<DtailTiketModel> getBook() {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String[] sqlSelect = {"idbooking", "judulfilm", "tanggal","waktu", "studio", "kursi", "jmlkursi"};
        String table = "dtailtiket";

        qb.setTables(table);
        Cursor c = qb.query(db, sqlSelect, null, null, null, null, null);

        final List<DtailTiketModel> result = new ArrayList<>();
        if (c.moveToFirst()) {
            do {
                result.add(new DtailTiketModel(
                        c.getString(c.getColumnIndex("idbooking")),
                        c.getString(c.getColumnIndex("judulfilm")),
                        c.getString(c.getColumnIndex("tanggal")),
                        c.getString(c.getColumnIndex("waktu")),
                        c.getString(c.getColumnIndex("studio")),
                        c.getString(c.getColumnIndex("kursi")),
                        c.getString(c.getColumnIndex("jmlkursi"))
                ));
            } while (c.moveToNext());
        }

        return result;
    };

    public void addDook(DtailTiketModel dtailTiketModel)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format("INSERT INTO dtailtiket(idbooking,judulfilm,tanggal,waktu,studio,kursi,jmlkursi) VALUES('%s','%s','%s','%s','%s','%s','%s');",
                dtailTiketModel.getIdbooking(),dtailTiketModel.getJudulfilm(),dtailTiketModel.getTanggal(),dtailTiketModel.getWaktu(),dtailTiketModel.getStudio(),dtailTiketModel.getKursi(),dtailTiketModel.getJmlkursi());
        db.execSQL(query);
    }
    public void delete(String dtailTiketModel)
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM dtailtiket");

        db.execSQL(query);
    }

    public void deleteByid(String bykey)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM dtailtiket WHERE idbooking='" + bykey + "'";
        db.execSQL(query);
    }
    public List<DtailTiketModel> SelectByid(String byid) {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        String table = "dtailtiket";
        qb.setTables(table);
        Cursor c=null;
        c = db.rawQuery("SELECT * FROM " + table + " WHERE idbooking='" + byid + "'" ,null);
        final List<DtailTiketModel> result = new ArrayList<>();
        if (c !=null) {
            if (c.moveToFirst()) {
                {
                result.add(new DtailTiketModel(
                    c.getString(c.getColumnIndex("idbooking")),
                    c.getString(c.getColumnIndex("judulfilm")),
                    c.getString(c.getColumnIndex("tanggal")),
                    c.getString(c.getColumnIndex("waktu")),
                    c.getString(c.getColumnIndex("studio")),
                    c.getString(c.getColumnIndex("kursi")),
                    c.getString(c.getColumnIndex("jmlkursi"))
                    ));
                }
            }
            c.close();
        }
        return result;
    };
}
