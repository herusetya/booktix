package com.setia.booktix.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.setia.booktix.Adapter.WaktuAdapter;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.Model.WaktuModel;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LatJadwal extends AppCompatActivity {
    private Toolbar toolbarJadwal;
    private String judul,gambar,kategori,durasi,deskripsi,jadwal,studio;
    public String time;
    private String iduser,username;
    private TextView judulFilm,durasiFilm,kategoriFilm,deskripsiFilm,jadwalFilm,studioFilm,waktuFilm;
    private ImageView poster;
    private Button PilihKursi;

    RecyclerView recyclerView;
    ArrayList<WaktuModel> modelArrayList = new ArrayList<>();
    WaktuAdapter waktuAdapter;
    String url = Server.URL_WAKTU;
    private String tag_json_obj = "json_obj_req";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_jadwal);

        if (getIntent() != null) {
            gambar = getIntent().getStringExtra("gambar");
            judul = getIntent().getStringExtra("judul");
            kategori = getIntent().getStringExtra("kategori");
            durasi = getIntent().getStringExtra("durasi");
            jadwal = getIntent().getStringExtra("jadwal");
            studio = getIntent().getStringExtra("studio");
            iduser = getIntent().getStringExtra("iduser");
            username = getIntent().getStringExtra("username");
        }

        poster = findViewById(R.id.gambarPoster);
        judulFilm = findViewById(R.id.judulFilmDetail);
        kategoriFilm = findViewById(R.id.kategoriFilmDetail);
        durasiFilm = findViewById(R.id.durasiFilmDetail);
        jadwalFilm = findViewById(R.id.txt_tanggal);
        studioFilm = findViewById(R.id.txt_studio);

        Glide.with(LatJadwal.this)
                .load(gambar)
                .into(poster);
        judulFilm.setText(judul);
        kategoriFilm.setText(kategori);
        durasiFilm.setText(durasi);
        jadwalFilm.setText(jadwal);
        studioFilm.setText(studio);

        ////////////////////////////////////////////////////////////////////////////////////////////

        recyclerView = findViewById(R.id.rv_waktu);
        modelArrayList = new ArrayList<>();
        loadWaktu();
        recyclerView.setLayoutManager(new GridLayoutManager(LatJadwal.this,1,GridLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        waktuAdapter = new WaktuAdapter(LatJadwal.this,modelArrayList);
        recyclerView.setAdapter(waktuAdapter);

        ////////////////////////////////////////////////////////////////////////////////////////////
        toolbarJadwal = findViewById(R.id.toolbarJadwal);
        toolbarJadwal.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbarJadwal.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ///////////////PILIH KURSI
        PilihKursi = findViewById(R.id.btn_kursi);
        PilihKursi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(time)) {
                    Toast.makeText(getApplicationContext(), "Tentukan Waktu Dulu", Toast.LENGTH_LONG).show();
                } else {
                        Intent intent = new Intent(LatJadwal.this, LatKursi.class);
                        intent.putExtra("judul", judulFilm.getText().toString());
                        intent.putExtra("tanggal", jadwalFilm.getText().toString());
                        intent.putExtra("waktu", time.trim());
                        intent.putExtra("studio", studioFilm.getText().toString());
                        intent.putExtra("iduser", iduser.trim());
                        intent.putExtra("username", username.trim());
                        startActivity(intent);
                }

            }
        });
        ///////////////
    }

    private void loadWaktu() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject data = jsonArray.getJSONObject(i);
                            WaktuModel model = new WaktuModel();
                            model.setWaktuTayang(data.getString("waktu"));
                            model.setHari(data.getString("hari"));
                            modelArrayList.add(model);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    waktuAdapter.notifyDataSetChanged();
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("volley", "error : " + error.getMessage());
            }
        });
        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }
}
