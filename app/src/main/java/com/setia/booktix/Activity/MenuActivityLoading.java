package com.setia.booktix.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.setia.booktix.R;

public class MenuActivityLoading extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zz_menuke2);

        Thread timer = new Thread(){
            @Override
            public void run() {
                try{
                    sleep(1000);
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    Intent intent = new Intent(MenuActivityLoading.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        timer.start();
    }
}
