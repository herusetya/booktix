package com.setia.booktix.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.setia.booktix.Fragment.AkunFragment;
import com.setia.booktix.Fragment.BantuanFragment;
import com.setia.booktix.Fragment.BerandaFragment;
import com.setia.booktix.Fragment.PesanFragment;
import com.setia.booktix.R;

public class MenuActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    public String iduser, username;
    TextView txtUsername;
    SharedPreferences sharedpreferences;
    public static final String TAG_ID = "id_user";
    public static final String TAG_USERNAME = "username";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        sharedpreferences = MenuActivity.this.getSharedPreferences(LoginActivity.my_shared_preferences, Context.MODE_PRIVATE);
        iduser = MenuActivity.this.getIntent().getStringExtra(TAG_ID);
        username = MenuActivity.this.getIntent().getStringExtra(TAG_USERNAME);
        txtUsername = findViewById(R.id.txt_USR);
        txtUsername.setText(username);
        loadFragment(new BerandaFragment());
        BottomNavigationView bottomNavigationView = findViewById(R.id.bn_main);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;
        switch (item.getItemId()){
            case R.id.home_menu:
                fragment = new BerandaFragment();
                break;
            case R.id.favorite_menu:
                fragment = new PesanFragment();
                break;
            case R.id.account_menu:
                fragment = new AkunFragment();
                break;
            case R.id.setting_menu:
                fragment = new BantuanFragment();
                break;
        }
        return loadFragment(fragment);
    }

}
