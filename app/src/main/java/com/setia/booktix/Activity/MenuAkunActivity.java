package com.setia.booktix.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MenuAkunActivity extends AppCompatActivity {

    private static final String TAG = MenuAkunActivity.class.getSimpleName();
    int success;
    String url = Server.URL_UPDATE;
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private String KEY_IDUSER = "id_user";
    private String KEY_USERNAME = "username";
    private String KEY_PASSWORD = "password";
    private String KEY_NAMA = "nama";
    private String KEY_NOMORHP = "nomorhp";
    String tag_json_obj = "json_obj_req";

    private EditText EDTusername,EDTpassword,EDTnama,EDTnomorhp;
    private TextView EDTid_user;
    private Button btn_simpan,btn_kembali;
    private String id_user,username,password,nama,nomorhp;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_menu_akunedit);
        if(getIntent()!= null){
            id_user = getIntent().getStringExtra("id_user");
            username = getIntent().getStringExtra("username");
            password = getIntent().getStringExtra("password");
            nama = getIntent().getStringExtra("nama");
            nomorhp = getIntent().getStringExtra("nomorhp");
        }
        EDTid_user = findViewById(R.id.txt_id_user);
        EDTusername = findViewById(R.id.txt_username);
        EDTpassword = findViewById(R.id.txt_password);
        EDTnama = findViewById(R.id.txt_nama);
        EDTnomorhp = findViewById(R.id.txt_nomorhp);

        EDTid_user.setText(id_user);
        EDTusername.setText(username);
        EDTpassword.setText(password);
        EDTnama.setText(nama);
        EDTnomorhp.setText(nomorhp);

        btn_simpan = findViewById(R.id.btnsimpan);
        btn_kembali = findViewById(R.id.btnkembali);
        btn_kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btn_simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateAkun();
            }
        });


    }
    private void updateAkun() {
        //menampilkan progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Update...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Response: " + response.toString());

                        try {
                            JSONObject jObj = new JSONObject(response);
                            success = jObj.getInt(TAG_SUCCESS);

                            if (success == 1) {
                                Log.e("v Add", jObj.toString());

                                Toast.makeText(MenuAkunActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                                berhasil();
                            } else {
                                Toast.makeText(MenuAkunActivity.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //menghilangkan progress dialog
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //menghilangkan progress dialog
                        loading.dismiss();

                        //menampilkan toast
                        Toast.makeText(MenuAkunActivity.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, error.getMessage().toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                //membuat parameters
                Map<String, String> params = new HashMap<String, String>();

                //menambah parameter yang di kirim ke web servis
                params.put(KEY_IDUSER, EDTid_user.getText().toString());
                params.put(KEY_USERNAME, EDTusername.getText().toString().trim());
                params.put(KEY_PASSWORD, EDTpassword.getText().toString().trim());
                params.put(KEY_NAMA, EDTnama.getText().toString().trim());
                params.put(KEY_NOMORHP, EDTnomorhp.getText().toString().trim());

                //kembali ke parameters
                Log.e(TAG, "" + params);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }
    private void berhasil(){
        startActivity(new Intent(MenuAkunActivity.this, MenuActivityLoading.class));
    }
}
