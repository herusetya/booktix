package com.setia.booktix.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.Database.Database2;
import com.setia.booktix.Model.DtailTiketModel;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LatPembayaran extends AppCompatActivity {
    Button buttonChoose,btn_mUtama;
    Toolbar toolbar;
    FloatingActionButton buttonUpload;
    ImageView imageView;
    TextView txt_name,txtJudul,txtTanggal,txtWaktu,txtStudio,txtKursi,txtJmhKursi,txttotal;
    Bitmap bitmap, decoded;
    int success;
    int PICK_IMAGE_REQUEST = 1;
    int bitmap_size = 60; // range 1 - 100
    private static final String TAG = LatPembayaran.class.getSimpleName();

    /* 10.0.2.2 adalah IP Address localhost Emulator Android Studio. Ganti IP Address tersebut dengan
    IP Address Laptop jika di RUN di HP/Genymotion. HP/Genymotion dan Laptop harus 1 jaringan! */
    String url = Server.URL_PEMBAYARAN;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private String KEY_GAMBAR = "image";
    private String KEY_USERNAME = "username";
    private String KEY_JUDULFILM = "judulfilm";
    private String KEY_TOTAL_BAYAR = "total_bayar";
    private String KEY_NO_KURSI = "no_kursi";
    private String KEY_IDUSER = "id_user";
    public String iduser;


    String tag_json_obj = "json_obj_req";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_pembayaran);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonChoose = findViewById(R.id.buttonChoose);
        buttonUpload = findViewById(R.id.buttonUpload1);
        txt_name = findViewById(R.id.editText);
        imageView = findViewById(R.id.imageView);
        btn_mUtama = findViewById(R.id.btn_mUtama);

        txtJudul = findViewById(R.id.txt_judul);
        txtTanggal = findViewById(R.id.txt_tanggal);
        txtWaktu = findViewById(R.id.txt_waktu);
        txtStudio = findViewById(R.id.txt_studio);
        txtKursi = findViewById(R.id.txt_kursi);
        txtJmhKursi = findViewById(R.id.txt_jmlKursi);
        txttotal = findViewById(R.id.txt_totalbayar);

        buttonChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadImage();

            }
        });
        Bundle bundle=getIntent().getExtras();
        String s=bundle.getString("username");
        txt_name.setText(s);
        iduser=bundle.getString("iduser");
        String judulF=bundle.getString("judulfilm");
        String tanggalF=bundle.getString("tanggal");
        String waktuF=bundle.getString("waktu");
        String studioF=bundle.getString("studio");
        String namaKrs=bundle.getString("namakrs");
        String tjmltiket=bundle.getString("jumlahkrs");
        String totalb=bundle.getString("totalbayar");
        txtJudul.setText(judulF);
        txtTanggal.setText(tanggalF);
        txtWaktu.setText(waktuF);
        txtStudio.setText(studioF);
        txtKursi.setText(namaKrs);
        txtJmhKursi.setText(tjmltiket);
        txttotal.setText(totalb);

        btn_mUtama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LatPembayaran.this, MenuActivityLoading.class));
                finish();
            }
        });
    }
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
    private void uploadImage() {
        //menampilkan progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e(TAG, "Response: " + response.toString());

                        try {
                            JSONObject jObj = new JSONObject(response);
                            success = jObj.getInt(TAG_SUCCESS);

                            if (success == 1) {
                                Log.e("v Add", jObj.toString());

                                Toast.makeText(LatPembayaran.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                                kosong();

                            } else {
                                Toast.makeText(LatPembayaran.this, jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //menghilangkan progress dialog
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //menghilangkan progress dialog
                        loading.dismiss();

                        //menampilkan toast
                        Toast.makeText(LatPembayaran.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                        Log.e(TAG, error.getMessage().toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                //membuat parameters
                Map<String, String> params = new HashMap<String, String>();

                //menambah parameter yang di kirim ke web servis
                params.put(KEY_GAMBAR, getStringImage(decoded));
                params.put(KEY_USERNAME, txt_name.getText().toString().trim());
                params.put(KEY_JUDULFILM, txtJudul.getText().toString().trim());
                params.put(KEY_TOTAL_BAYAR, txttotal.getText().toString().trim());
                params.put(KEY_NO_KURSI, txtKursi.getText().toString().trim());
                params.put(KEY_IDUSER, iduser.trim());
                //kembali ke parameters
                Log.e(TAG, "" + params);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {
                //mengambil fambar dari Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                // 512 adalah resolusi tertinggi setelah image di resize, bisa di ganti.
                setToImageView(getResizedBitmap(bitmap, 512));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void kosong() {
        imageView.setImageResource(0);
        final Database2 dbku = new Database2(LatPembayaran.this);
        DtailTiketModel bm = new DtailTiketModel();
        bm.setIdbooking(iduser.trim());
        bm.setJudulfilm(txtJudul.getText().toString());
        bm.setTanggal(txtTanggal.getText().toString());
        bm.setWaktu(txtWaktu.getText().toString());
        bm.setStudio(txtStudio.getText().toString());
        bm.setKursi(txtKursi.getText().toString());
        bm.setJmlkursi(txtJmhKursi.getText().toString());
        dbku.addDook(bm);
        startActivity(new Intent(LatPembayaran.this, MenuActivityLoading.class));
        finish();
    }

    private void setToImageView(Bitmap bmp) {
        //compress image
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, bitmap_size, bytes);
        decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));

        //menampilkan gambar yang dipilih dari camera/gallery ke ImageView
        imageView.setImageBitmap(decoded);
    }

    // fungsi resize image
    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
