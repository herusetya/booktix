package com.setia.booktix.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.setia.booktix.Adapter.KursiAdapter;
import com.setia.booktix.Controller.AppController;
import com.setia.booktix.Model.KursiModel;
import com.setia.booktix.R;
import com.setia.booktix.Util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LatKursi extends AppCompatActivity {
    ArrayList<String> itemlistKRS;
    ArrayAdapter<String> adapterKRS;
    public ListView lvKRS;
    public String NO_KRS;

    RecyclerView recyclerView;
    String url = Server.URL_KURSI;
    KursiAdapter kursiAdapter;
    private String tag_json_obj = "json_obj_req";

    private TextView txt_juduldikursi,tanggal,waktu,studio,txtjumlahkursi,txt_nokursi,txtiduser,txtusername,txthargakursi;
    private Button btnpesanS;
    private int jmlahkursi=0;
    private EditText Edt_cekJML;
    ArrayList<KursiModel> modelArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_kursi);
        /////////////////////////////////////////////////////////////
        recyclerView = findViewById(R.id.rv_kursi);
        modelArrayList = new ArrayList<>();
        loadKursi();
        recyclerView.setLayoutManager(new GridLayoutManager(LatKursi.this,5,GridLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        kursiAdapter = new KursiAdapter(LatKursi.this,modelArrayList);
        recyclerView.setAdapter(kursiAdapter);
        //////////////////////////////////////////////////////////////
        txt_juduldikursi = findViewById(R.id.txt_juduldikursi);
        tanggal = findViewById(R.id.txt_tanggaldikursi);
        waktu = findViewById(R.id.txt_waktudikursi);
        studio = findViewById(R.id.txt_studiodikursi);
        txtiduser = findViewById(R.id.TXTiduser);
        txtusername = findViewById(R.id.TXTusername);

        Bundle bundle=getIntent().getExtras();
        String judulF=bundle.getString("judul");
        txt_juduldikursi.setText(judulF);
        String tanggalF=bundle.getString("tanggal");
        tanggal.setText(tanggalF);
        String waktuF=bundle.getString("waktu");
        waktu.setText(waktuF);
        String studioF=bundle.getString("studio");
        studio.setText(studioF);
        String iduser=bundle.getString("iduser");
        txtiduser.setText(iduser);
        String username=bundle.getString("username");
        txtusername.setText(username);
        ///////////////
        lvKRS = findViewById(R.id.listviewKRS);
        itemlistKRS = new ArrayList<>();
        adapterKRS = new ArrayAdapter<String>(LatKursi.this, android.R.layout.simple_list_item_multiple_choice, itemlistKRS);
        lvKRS.setAdapter(adapterKRS);
        ////////////////
        btnpesanS = findViewById(R.id.btn_pesanS);
        btnpesanS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(NO_KRS)) {
                    Toast.makeText(getApplicationContext(), "Tentukan Tempat Duduk Dulu", Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(LatKursi.this, LatDetailPemesanan.class);
                    intent.putExtra("iduser", txtiduser.getText().toString());
                    intent.putExtra("username", txtusername.getText().toString());
                    intent.putExtra("judulfilm", txt_juduldikursi.getText().toString());
                    intent.putExtra("tanggal", tanggal.getText().toString());
                    intent.putExtra("waktu", waktu.getText().toString());
                    intent.putExtra("studio", studio.getText().toString());
                    intent.putExtra("namakrs", itemlistKRS.toString());
                    intent.putExtra("jumlahkrs", txtjumlahkursi.getText().toString());
                    startActivity(intent);
                }
            }
        });

    }
    public void loadTambahnoKrs(){
        itemlistKRS.add(NO_KRS.trim());
        adapterKRS.notifyDataSetChanged();
        txt_nokursi = findViewById(R.id.txtnoKrs);
        txt_nokursi.setText(itemlistKRS.toString());
    }
    public void loadKurangnoKrs(){
        int count = lvKRS.getCount();
        for (int item = count - 1;item >= 0; item--) {
            adapterKRS.remove(itemlistKRS.get(item));
        }
        adapterKRS.notifyDataSetChanged();
        txt_nokursi = findViewById(R.id.txtnoKrs);
        txt_nokursi.setText(itemlistKRS.toString());
    }
    public void tambahAg(){//perintah tombol tambah
        if(jmlahkursi==5){
            Toast.makeText(this,"Jumlah Kursi maximal 5",Toast.LENGTH_SHORT).show();
            onBackPressed();
            return;
        }
        jmlahkursi = jmlahkursi+1 ;
        display(jmlahkursi);
    }
    public void kurangAg(){//perintah tombol kurang
        if (jmlahkursi==0){
            Toast.makeText(this,"Jumlah Kursi minimal 1",Toast.LENGTH_SHORT).show();
            return;
        }
        jmlahkursi = jmlahkursi -1;
        display(jmlahkursi);
    }
    private void display( int number) {
        txtjumlahkursi = findViewById(R.id.jumlahKrs);
        txtjumlahkursi.setText("" + number);
        txthargakursi = findViewById(R.id.hargaKrs);
        int jumlahkursi = number;
        int hargakursi=25000;
        int jk = jumlahkursi;
        int hk = (hargakursi);
        int totalhargakursi = (jk * hk);
        txthargakursi.setText("Rp. " + totalhargakursi + " rb");
    }
    private void loadKursi() {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            KursiModel model = new KursiModel();
                            model.setNomorKursi(data.getString( "no_kursi" ));
                            model.setStatus(data.getString( "status" ));
                            model.setStudio(data.getString( "studio" ));
                            modelArrayList.add(model);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    kursiAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("studio",studio.getText().toString());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);
    }

}