package com.setia.booktix.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.setia.booktix.R;

public class LatDetailPemesanan extends AppCompatActivity {
    TextView idbooking,judulfilm,tanggal,waktu,studio,kursi,jmlKURSI;
    TextView hargatiket,biayaplyann,diskon,total;
    Button btnbayar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_detail_pesanan);

        Bundle bundle=getIntent().getExtras();
        String idbookingF=bundle.getString("iduser");
        final String username=bundle.getString("username");
        String judulF=bundle.getString("judulfilm");
        String tanggalF=bundle.getString("tanggal");
        String waktuF=bundle.getString("waktu");
        String studioF=bundle.getString("studio");
        String namaKrs=bundle.getString("namakrs");
        final String tjmltiket=bundle.getString("jumlahkrs");

        idbooking = findViewById(R.id.idbooking1);
        idbooking.setText(idbookingF);
        judulfilm = findViewById(R.id.judulfilm1);
        judulfilm.setText(judulF);
        tanggal = findViewById(R.id.tanggal1);
        tanggal.setText(tanggalF);
        waktu = findViewById(R.id.waktu1);
        waktu.setText(waktuF);
        studio = findViewById(R.id.studio1);
        studio.setText(studioF);
        kursi = findViewById(R.id.kursi1);
        kursi.setText(namaKrs);

        /*jmlKURSI = findViewById(R.id.jmlKrs);
        jmlKURSI.setText(tjmltiket);*/

        hargatiket = findViewById(R.id.hargatiket1);
        biayaplyann = findViewById(R.id.biayaplyann1);
        diskon = findViewById(R.id.diskon1);
        total = findViewById(R.id.total1);


        String jumlahkursi = tjmltiket.trim();
        int hargakursi=25000;
        int uangplynn=2000;
        int diskonl=25000;

        int jk = Integer.parseInt(jumlahkursi);
        int hk = (hargakursi);
        int plynn = (uangplynn);

        int totalhargakursi = (jk * hk);
        hargatiket.setText("Rp. " + totalhargakursi);

        int totalplynn = (totalhargakursi + plynn);
        total.setText("Rp. " + totalplynn);


        if (totalhargakursi >=125000){
            int dskon = (totalhargakursi - diskonl);
            total.setText("Rp. " + dskon);
            biayaplyann.setText("Rp. " + uangplynn);
            if (dskon >= 0) {
                diskon.setText("Rp. 25000");
            }
        } else if(totalhargakursi <=125000){
            total.setText("Rp. " + totalplynn);
            biayaplyann.setText("Rp. " + uangplynn);
            diskon.setText("Rp. 0");
        }

        btnbayar = findViewById(R.id.btnbayar);
        btnbayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LatDetailPemesanan.this,LatPembayaran.class);
                intent.putExtra("iduser",idbooking.getText().toString());
                intent.putExtra("username",username.trim());
                intent.putExtra("judulfilm",judulfilm.getText().toString());
                intent.putExtra("tanggal",tanggal.getText().toString());
                intent.putExtra("waktu",waktu.getText().toString());
                intent.putExtra("studio",studio.getText().toString());
                intent.putExtra("namakrs",kursi.getText().toString());
                intent.putExtra("jumlahkrs",tjmltiket.trim());
                intent.putExtra("totalbayar",total.getText().toString());
                startActivity(intent);
            }
        });



    }
}
