package com.setia.booktix.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.setia.booktix.Database.Database2;
import com.setia.booktix.Model.DtailTiketModel;
import com.setia.booktix.R;

public class LatPesanManual extends AppCompatActivity {
    private EditText edtidbooking,edtjudulfilm,edttanggal,edtwaktu,edtstudio,edtkursi;
    private Button btnBooking,btnLihat;
    private int jmlahkursi=0;
    private TextView txtjumlahkursi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_pesan_manual);

        edtidbooking = findViewById(R.id.edidbooking);
        edtjudulfilm = findViewById(R.id.edjudulfilm);
        edttanggal = findViewById(R.id.edtanggal);
        edtwaktu = findViewById(R.id.edwaktu);
        edtstudio = findViewById(R.id.edstudio);
        edtkursi = findViewById(R.id.edkursi);
        txtjumlahkursi = findViewById(R.id.txtjumlahkursi);
        btnBooking = findViewById(R.id.btnBooking);


        final Database2 dbku = new Database2(LatPesanManual.this);

        btnBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DtailTiketModel bm = new DtailTiketModel();
                bm.setIdbooking(edtidbooking.getText().toString());
                bm.setJudulfilm(edtjudulfilm.getText().toString());
                bm.setTanggal(edttanggal.getText().toString());
                bm.setWaktu(edtwaktu.getText().toString());
                bm.setStudio(edtstudio.getText().toString());
                bm.setKursi(edtkursi.getText().toString());
                bm.setJmlkursi(txtjumlahkursi.getText().toString());
                dbku.addDook(bm);
            }
        });
    }

    public void increment(View view){//perintah tombol tambah
        if(jmlahkursi==5){
            Toast.makeText(this,"Jumlah Kursi maximal 5",Toast.LENGTH_SHORT).show();
            return;
        }
        jmlahkursi = jmlahkursi+1 ;
        display(jmlahkursi);
    }
    public void decrement(View view){//perintah tombol tambah
        if (jmlahkursi==1){
            Toast.makeText(this,"Jumlah Kursi minimal 1",Toast.LENGTH_SHORT).show();
            return;
        }
        jmlahkursi = jmlahkursi -1;
        display(jmlahkursi);
    }
    private void display(int number) {
        TextView txtjumlahkursi = findViewById(R.id.txtjumlahkursi);
        txtjumlahkursi.setText("" + number);
    }
}
