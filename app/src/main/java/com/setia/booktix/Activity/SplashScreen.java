package com.setia.booktix.Activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.setia.booktix.R;

public class SplashScreen extends AppCompatActivity {
    TextView titleSplash;
    Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        titleSplash = findViewById(R.id.titleSplash);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/fontku.otf");
        titleSplash.setTypeface(typeface);

        Thread timer = new Thread(){
            @Override
            public void run() {
                try{
                    sleep(3000);
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        };

        timer.start();
    }
}
