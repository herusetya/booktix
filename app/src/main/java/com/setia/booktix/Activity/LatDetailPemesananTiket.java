package com.setia.booktix.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.setia.booktix.R;

public class LatDetailPemesananTiket extends AppCompatActivity {
    TextView idbooking,judulfilm,tanggal,waktu,studio,kursi;
    TextView hargatiket,biayaplyann,diskon,total;
    String tidbooking,tjudulfilm,ttanggal,twaktu,tstudio,tkursi,tjmltiket;
    Button btnbayar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lat_menu_pemesanan_detail_tiket);
        if(getIntent()!= null){
            tidbooking = getIntent().getStringExtra("idbooking");
            tjudulfilm = getIntent().getStringExtra("judulfilm");
            ttanggal = getIntent().getStringExtra("tanggal");
            twaktu = getIntent().getStringExtra("waktu");
            tstudio = getIntent().getStringExtra("studio");
            tkursi = getIntent().getStringExtra("kursi");
            tjmltiket = getIntent().getStringExtra("jmlkursi");
        }
        idbooking = findViewById(R.id.idbooking1);
        idbooking.setText(tidbooking);

        judulfilm = findViewById(R.id.judulfilm1);
        judulfilm.setText(tjudulfilm);
        tanggal = findViewById(R.id.tanggal1);
        tanggal.setText(ttanggal);
        waktu = findViewById(R.id.waktu1);
        waktu.setText(twaktu);
        studio = findViewById(R.id.studio1);
        studio.setText(tstudio);
        kursi = findViewById(R.id.kursi1);
        kursi.setText(tkursi);

        hargatiket = findViewById(R.id.hargatiket1);
        biayaplyann = findViewById(R.id.biayaplyann1);
        diskon = findViewById(R.id.diskon1);
        total = findViewById(R.id.total1);


        String jumlahkursi = tjmltiket.trim();
        int hargakursi=25000;
        int uangplynn=2000;
        int diskonl=25000;

        int jk = Integer.parseInt(jumlahkursi);
        int hk = (hargakursi);
        int plynn = (uangplynn);

        int totalhargakursi = (jk * hk);
        hargatiket.setText("Rp. " + totalhargakursi);

        int totalplynn = (totalhargakursi + plynn);
        total.setText("Rp. " + totalplynn);


        if (totalhargakursi >=125000){
            int dskon = (totalhargakursi - diskonl);
            total.setText("Rp. " + dskon);
            biayaplyann.setText("Rp. " + uangplynn);
            if (dskon >= 0) {
                diskon.setText("Rp. 25000");
            }
        } else if(totalhargakursi <=125000){
            total.setText("Rp. " + totalplynn);
            biayaplyann.setText("Rp. " + uangplynn);
            diskon.setText("Rp. 0");
        }

    }
}
