package com.setia.booktix.Model;

public class PosterFilmModel {

    public PosterFilmModel(){
    }

    public PosterFilmModel(String judulFilm){
        this.judulFilm = judulFilm;
    }

    public String getJudulFilm() {
        return judulFilm;
    }

    public void setJudulFilm(String judulFilm){
        this.judulFilm = judulFilm;
    }

    String judulFilm;

    public String getKategori() { return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    String kategori;

    public String getDurasi() {
        return durasi;
    }

    public void setDurasi(String durasi) {
        this.durasi = durasi;
    }

    String durasi;

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    String gambar;

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    String deskripsi;

    public String getJadwal() {
        return jadwal;
    }

    public void setJadwal(String jadwal) {
        this.jadwal = jadwal;
    }

    String jadwal;

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }

    String studio;

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String id_user,username;

}
