package com.setia.booktix.Model;

public class BookingModel {
    String iduser,idkursi,idfilm;

    public BookingModel(String iduser, String idkursi, String idfilm) {
        this.iduser = iduser;
        this.idkursi = idkursi;
        this.idfilm = idfilm;
    }

    public BookingModel() {
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public String getIdkursi() {
        return idkursi;
    }

    public void setIdkursi(String idkursi) {
        this.idkursi = idkursi;
    }

    public String getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(String idfilm) {
        this.idfilm = idfilm;
    }
}
