package com.setia.booktix.Model;

public class DtailTiketModel {
    String idbooking,judulfilm,tanggal,waktu,studio,kursi,jmlkursi;

    public DtailTiketModel (String idbooking, String judulfilm, String tanggal, String waktu, String studio, String kursi, String jmlkursi){

        this.idbooking = idbooking;
        this.judulfilm = judulfilm;
        this.tanggal = tanggal;
        this.waktu = waktu;
        this.studio = studio;
        this.kursi = kursi;
        this.jmlkursi = jmlkursi;
    }

    public DtailTiketModel() {

    }



    public String getIdbooking() {
        return idbooking;
    }

    public void setIdbooking(String idbooking) {
        this.idbooking = idbooking;
    }

    public String getJudulfilm() {
        return judulfilm;
    }

    public void setJudulfilm(String judulfilm) {
        this.judulfilm = judulfilm;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getStudio() {
        return studio;
    }

    public void setStudio(String studio) {
        this.studio = studio;
    }


    public String getKursi() {
        return kursi;
    }

    public void setKursi(String kursi) {
        this.kursi = kursi;
    }


    public String getJmlkursi() { return jmlkursi; }

    public void setJmlkursi(String jmlkursi) {
        this.jmlkursi = jmlkursi;
    }
}
