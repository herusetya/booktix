package com.setia.booktix.Model;

public class WaktuModel {
    String waktuTayang,hari;

    public WaktuModel() {
    }

    public WaktuModel(String waktuTayang, String hari) {
        this.waktuTayang = waktuTayang;
        this.hari = hari;
    }

    public String getWaktuTayang() {
        return waktuTayang;
    }

    public void setWaktuTayang(String waktuTayang) {
        this.waktuTayang = waktuTayang;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

}
